package main

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/go-chi/chi"
	"github.com/jackc/pgx/v4/pgxpool"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"

	"gitlab.com/kokkzlya/evermos/app/evermos"
	"gitlab.com/kokkzlya/evermos/app/webapi"
)

func main() {
	viper.AutomaticEnv()
	viper.SetEnvPrefix("evermos")
	log.SetFormatter(&log.JSONFormatter{})
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	postgresURL := viper.GetString("db_url")
	port := viper.GetInt("http_listen_port")
	// conn, err := pgxpool.Connect(ctx, "postgres://evermos:evermos@0.0.0.0:5432/evermos?sslmode=disable")
	conn, err := pgxpool.Connect(ctx, postgresURL)
	if err != nil {
		panic(err)
	}

	app := webapi.New()
	app.CustOrderSrv = evermos.NewDefaultCustOrderService()
	app.CustomerSrv = evermos.NewDefaultCustomerService()
	app.ProductSrv = evermos.NewDefaultProductService()

	r := chi.NewRouter()
	r.Use(evermos.CreateScopedSessionMiddleware(&evermos.PgScopedSessionFactory{}, conn))
	r.Mount("/api", app.Router())

	server := http.Server{
		Addr:    fmt.Sprintf(":%d", port),
		Handler: r,
	}

	// start the server asynchronously
	go func() {
		log.Info("Starting the server")
		if err := server.ListenAndServe(); err != nil {
			log.Error(err)
		}
	}()

	// gracefully shutdown
	sigCh := make(chan os.Signal, 1)
	signal.Notify(sigCh, syscall.SIGTERM, syscall.SIGINT)
	<-sigCh

	log.Info("Shutting down the server")
	toCtx, toCancel := context.WithTimeout(ctx, 7*time.Second)
	defer toCancel()
	if err = server.Shutdown(toCtx); err != nil {
		if err = server.Close(); err != nil {
			log.Warning(err)
		}
	}

	// rr := httptest.NewRecorder()
	// req := httptest.NewRequest(http.MethodGet, "/api/projects", nil)
	// r.ServeHTTP(rr, req)

	log.Info("The server is shutted down")
}
