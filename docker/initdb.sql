CREATE TABLE IF NOT EXISTS customer (
    id       TEXT PRIMARY KEY,
    name     TEXT NOT NULL,
    username TEXT UNIQUE NOT NULL,
    email    TEXT UNIQUE NOT NULL,
    created  TIMESTAMP NOT NULL,
    updated  TIMESTAMP NOT NULL
);

CREATE TABLE IF NOT EXISTS product (
    id      TEXT PRIMARY KEY,
    name    TEXT NOT NULL,
    price   DECIMAL DEFAULT 0,
    stock   INT DEFAULT 0,
    created TIMESTAMP NOT NULL,
    updated TIMESTAMP NOT NULL
);

CREATE TABLE IF NOT EXISTS customer_order (
    id          TEXT PRIMARY KEY,
    customer_id TEXT,
    created     TIMESTAMP NOT NULL,
    updated     TIMESTAMP NOT NULL,
    CONSTRAINT fk_customer_order_customer
        FOREIGN KEY(customer_id) REFERENCES customer(id)
);

CREATE TABLE IF NOT EXISTS customer_order_item (
    id         INT NOT NULL,
    order_id   TEXT NOT NULL,
    product_id TEXT NOT NULL,
    quantity   INT NOT NULL,
    price      DECIMAL DEFAULT 0,
    PRIMARY KEY(id, order_id),
    CONSTRAINT fk_customer_order_item_customer_order
        FOREIGN KEY(order_id) REFERENCES customer_order(id),
    CONSTRAINT fk_customer_order_item_product
        FOREIGN KEY(product_id) REFERENCES product(id)
);
