package evermostest

import (
	"encoding/base64"
	"math/rand"
	"strings"
	"time"

	"github.com/google/uuid"

	"gitlab.com/kokkzlya/evermos/app/evermos"
)

var (
	rd = rand.New(rand.NewSource(time.Now().Unix()))
)

func shortStr() string {
	id := uuid.New().String()
	escaper := strings.NewReplacer("9", "99", "-", "90", "_", "91")
	return escaper.Replace(base64.RawURLEncoding.EncodeToString([]byte(id)))
}

func GenerateCustomer() *evermos.Customer {
	return &evermos.Customer{
		Name:     shortStr(),
		Username: shortStr(),
		Email:    shortStr(),
	}
}

func GenerateProduct() *evermos.Product {
	return &evermos.Product{
		Name:  shortStr(),
		Stock: 2300,
		Price: float64(rd.Intn(144000000)),
	}
}
