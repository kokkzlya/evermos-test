module gitlab.com/kokkzlya/evermos

go 1.15

require (
	github.com/go-chi/chi v1.5.4
	github.com/google/uuid v1.2.0
	github.com/jackc/pgconn v1.8.1
	github.com/jackc/pgx/v4 v4.11.0
	github.com/pkg/errors v0.9.1 // indirect
	github.com/sirupsen/logrus v1.4.2
	github.com/spf13/viper v1.7.1
	github.com/stretchr/testify v1.7.0
	golang.org/x/crypto v0.0.0-20210506145944-38f3c27a63bf // indirect
	golang.org/x/text v0.3.6 // indirect
)
