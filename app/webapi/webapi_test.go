package webapi

import (
	"bytes"
	"context"
	"encoding/json"
	"math/rand"
	"net/http"
	"net/http/httptest"
	"os"
	"sync"
	"testing"
	"time"

	"github.com/go-chi/chi"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/spf13/viper"
	"github.com/stretchr/testify/require"

	"gitlab.com/kokkzlya/evermos/app/evermos"
	"gitlab.com/kokkzlya/evermos/internal/app/evermostest"
)

var (
	conn *pgxpool.Pool
)

func TestMain(m *testing.M) {
	viper.AutomaticEnv()
	viper.SetEnvPrefix("evermos")
	postgresURL := viper.GetString("db_url")
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// prepare webapi App
	var err error
	conn, err = pgxpool.Connect(ctx, postgresURL)
	if err != nil {
		panic(postgresURL)
	}
	os.Exit(m.Run())
}

func TestOrderItems(t *testing.T) {
	subtests := []struct {
		scenario string
		fn       func(*testing.T)
	}{
		{
			scenario: "test order items using handler",
			fn:       testOrderItemsUsingHandler,
		},
		{
			scenario: "test order items using server",
			fn:       testOrderItemsUsingServer,
		},
	}

	for _, test := range subtests {
		t.Run(test.scenario, test.fn)

		session := &evermos.PgScopedSession{Conn: conn}
		tx, _ := session.Begin(context.Background())
		_, _ = tx.Exec(context.Background(), "DELETE FROM customer_order_item")
		_, _ = tx.Exec(context.Background(), "DELETE FROM customer_order")
		_, _ = tx.Exec(context.Background(), "DELETE FROM product")
		_, _ = tx.Exec(context.Background(), "DELETE FROM customer")
		_ = tx.Commit(context.Background())
	}
}

func testOrderItemsUsingHandler(t *testing.T) {
	app := New()
	app.CustOrderSrv = evermos.NewDefaultCustOrderService()
	app.CustomerSrv = evermos.NewDefaultCustomerService()
	app.ProductSrv = evermos.NewDefaultProductService()

	session := &evermos.PgScopedSession{Conn: conn}
	r := chi.NewRouter()
	r.Use(evermos.CreateScopedSessionMiddleware(&evermos.PgScopedSessionFactory{}, conn))
	r.Mount("/api", app.Router())

	// lets us create 70 customers
	nCusts := 70
	customers := make([]*evermos.Customer, nCusts)
	for i := range customers {
		cust := evermostest.GenerateCustomer()
		b, _ := json.Marshal(cust)
		rr := httptest.NewRecorder()
		req := httptest.NewRequest(http.MethodPost, "/api/customers", bytes.NewBuffer(b))
		r.ServeHTTP(rr, req)

		err := json.NewDecoder(rr.Body).Decode(&cust)
		require.NoError(t, err)
		customers[i] = cust
	}

	// lets us create a product that will be abused by the 70 customers
	nProds := 4
	products := make([]*evermos.Product, nProds)
	for i := range products {
		p := evermostest.GenerateProduct()
		b, _ := json.Marshal(p)
		rr := httptest.NewRecorder()
		req := httptest.NewRequest(http.MethodPost, "/api/products", bytes.NewBuffer(b))
		r.ServeHTTP(rr, req)

		err := json.NewDecoder(rr.Body).Decode(&p)
		require.NoError(t, err)
		products[i] = p
	}

	// lets us spam order the product
	// prepare the orders
	rd := rand.New(rand.NewSource(time.Now().Unix()))
	nOrders := 5000
	orders := make([]*evermos.CustOrder, nOrders)
	for i := 0; i < nOrders; i++ {
		custOrder := &evermos.CustOrder{
			ID:       uuid.New().String(),
			Customer: customers[i%nCusts],
			Items:    make([]*evermos.CustOrderItem, nProds),
		}
		for j := 0; j < nProds; j++ {
			custOrder.Items[j] = &evermos.CustOrderItem{
				Product: products[j],
				Qty:     rd.Intn(70),
				Price:   products[j].Price,
			}
		}

		orders[i] = custOrder
	}

	// lets us create 24 requesters to make request our API
	requesterNum := 24
	requesterCh := make(chan func(*evermos.CustOrder), requesterNum)
	requesterFunc := func(custOrder *evermos.CustOrder) {
		b, _ := json.Marshal(custOrder)
		rr := httptest.NewRecorder()
		req := httptest.NewRequest(http.MethodPost, "/api/orders", bytes.NewBuffer(b))
		r.ServeHTTP(rr, req)
		// we only care if there is sql error
		require.Less(t, rr.Code, http.StatusInternalServerError)
	}
	for i := 0; i < requesterNum; i++ {
		requesterCh <- requesterFunc
	}

	wg := sync.WaitGroup{}
	wg.Add(nOrders)
	// then blindly bulk the requests without hoping that the response code shall be 201
	for i := 0; i < nOrders; i++ {
		r := <-requesterCh
		go func(requester func(*evermos.CustOrder), custOrder *evermos.CustOrder) {
			defer func() { requesterCh <- requester }()

			requester(custOrder)
			wg.Done()
		}(r, orders[i])
	}

	wg.Wait()

	// now assert the stock of the products, they should not be negative value
	for _, p := range products {
		ctx := context.WithValue(context.Background(), evermos.CKScopedSession, session)
		_, _ = session.Begin(ctx)
		xProduct, err := app.ProductSrv.FindByID(ctx, p.ID)
		require.NoError(t, err)
		require.GreaterOrEqual(t, xProduct.Stock, 0)
		_ = session.Tx.Commit(ctx)
	}
}

func testOrderItemsUsingServer(t *testing.T) {
	app := New()
	app.CustOrderSrv = evermos.NewDefaultCustOrderService()
	app.CustomerSrv = evermos.NewDefaultCustomerService()
	app.ProductSrv = evermos.NewDefaultProductService()

	session := &evermos.PgScopedSession{Conn: conn}
	r := chi.NewRouter()
	r.Use(evermos.CreateScopedSessionMiddleware(&evermos.PgScopedSessionFactory{}, conn))
	r.Mount("/api", app.Router())
	ts := httptest.NewServer(r)
	defer ts.Close()

	// lets us create 70 customers
	nCusts := 70
	customers := make([]*evermos.Customer, nCusts)
	for i := range customers {
		cust := evermostest.GenerateCustomer()
		b, _ := json.Marshal(cust)
		resp, err := http.Post(ts.URL+"/api/customers", "application/json", bytes.NewBuffer(b))
		require.NoError(t, err)
		defer resp.Body.Close()
		err = json.NewDecoder(resp.Body).Decode(&cust)
		require.NoError(t, err)
		customers[i] = cust
	}

	// lets us create a product that will be abused by the 70 customers
	nProds := 4
	products := make([]*evermos.Product, nProds)
	for i := range products {
		p := evermostest.GenerateProduct()
		b, _ := json.Marshal(p)
		resp, err := http.Post(ts.URL+"/api/products", "application/json", bytes.NewBuffer(b))
		require.NoError(t, err)
		defer resp.Body.Close()
		err = json.NewDecoder(resp.Body).Decode(&p)
		require.NoError(t, err)
		products[i] = p
	}

	// lets us spam order the product
	// prepare the orders
	rd := rand.New(rand.NewSource(time.Now().Unix()))
	nOrders := 5000
	orders := make([]*evermos.CustOrder, nOrders)
	for i := 0; i < nOrders; i++ {
		custOrder := &evermos.CustOrder{
			ID:       uuid.New().String(),
			Customer: customers[i%nCusts],
			Items:    make([]*evermos.CustOrderItem, nProds),
		}
		for j := 0; j < nProds; j++ {
			custOrder.Items[j] = &evermos.CustOrderItem{
				Product: products[j],
				Qty:     rd.Intn(70),
				Price:   products[j].Price,
			}
		}

		orders[i] = custOrder
	}

	// lets us create 24 requesters to make request our API
	requesterNum := 24
	requesterCh := make(chan func(*evermos.CustOrder), requesterNum)
	requesterFunc := func(custOrder *evermos.CustOrder) {
		b, _ := json.Marshal(custOrder)
		resp, err := http.Post(ts.URL+"/api/orders", "application/json", bytes.NewBuffer(b))
		require.NoError(t, err)
		defer resp.Body.Close()
		// we only care if there is sql error
		require.Less(t, resp.StatusCode, http.StatusInternalServerError)
	}
	for i := 0; i < requesterNum; i++ {
		requesterCh <- requesterFunc
	}

	wg := sync.WaitGroup{}
	wg.Add(nOrders)
	// then blindly bulk the requests without hoping that the response code shall be 201
	for i := 0; i < nOrders; i++ {
		r := <-requesterCh
		go func(requester func(*evermos.CustOrder), custOrder *evermos.CustOrder) {
			defer func() { requesterCh <- requester }()

			requester(custOrder)
			wg.Done()
		}(r, orders[i])
	}

	wg.Wait()

	// now assert the stock of the products, they should not be negative value
	for _, p := range products {
		ctx := context.WithValue(context.Background(), evermos.CKScopedSession, session)
		_, _ = session.Begin(ctx)
		xProduct, err := app.ProductSrv.FindByID(ctx, p.ID)
		require.NoError(t, err)
		require.GreaterOrEqual(t, xProduct.Stock, 0)
		_ = session.Tx.Commit(ctx)
	}
}
