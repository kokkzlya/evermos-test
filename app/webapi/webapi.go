package webapi

import (
	"encoding/json"
	"errors"
	"net/http"

	"github.com/go-chi/chi"
	log "github.com/sirupsen/logrus"

	"gitlab.com/kokkzlya/evermos/app/evermos"
)

type App struct {
	CustOrderSrv evermos.CustOrderService
	CustomerSrv  evermos.CustomerService
	ProductSrv   evermos.ProductService
}

func New() *App {
	api := &App{}
	return api
}

func (a *App) Router() chi.Router {
	r := chi.NewRouter()
	r.Get("/", a.Hello)

	r.Post("/customers", a.CreateCustomer)
	r.Get("/customers", a.FindCustomers)
	r.Get("/customers/{username}", a.FindCustomerByUsername)

	r.Post("/products", a.CreateProduct)
	r.Get("/products", a.FindProducts)
	r.Get("/products/{product_id}", a.FindProductByID)

	r.Post("/orders", a.CreateCustomerOrder)
	r.Get("/orders", a.FindCustomerOrders)
	r.Get("/orders/{order_id}", a.FindCustOrderByID)

	return r
}

func (a *App) Hello(w http.ResponseWriter, r *http.Request) {
	a.sendJSON(w, http.StatusOK, map[string]interface{}{
		"message": "Konnichi-wa, Sekai!",
	})
	// http.Error(w, "Oops! Something went wrong in server.", http.StatusInternalServerError)
}

//
// Customer handlers
//

func (a *App) CreateCustomer(w http.ResponseWriter, r *http.Request) {
	var cust *evermos.Customer
	err := json.NewDecoder(r.Body).Decode(&cust)
	if err != nil {
		a.sendJSON(w, http.StatusBadRequest, map[string]interface{}{
			"error": map[string]interface{}{
				"message": err.Error(),
			},
		})
		return
	}

	err = a.CustomerSrv.Create(r.Context(), cust)
	if err != nil {
		log.WithFields(log.Fields{
			"customer": cust,
			"error":    err,
		}).Error()
		http.Error(w, "Oops! Something went wrong in server.", http.StatusInternalServerError)
		return
	}

	a.sendJSON(w, http.StatusCreated, cust)
}

func (a *App) FindCustomerByUsername(w http.ResponseWriter, r *http.Request) {
	username := chi.URLParam(r, "username")
	cust, err := a.CustomerSrv.FindByUsername(r.Context(), username)
	switch {
	case errors.Is(err, evermos.ErrNotFound):
		a.sendJSON(w, http.StatusNotFound, map[string]interface{}{
			"error": map[string]interface{}{
				"message": err.Error(),
			},
		})
		return

	case err != nil:
		log.WithFields(log.Fields{
			"customer_id": username,
			"error":       err,
		}).Error()
		http.Error(w, "Oops! Something went wrong in server.", http.StatusInternalServerError)
		return
	}

	a.sendJSON(w, http.StatusOK, cust)
}

func (a *App) FindCustomers(w http.ResponseWriter, r *http.Request) {
	customers, err := a.CustomerSrv.FindMany(r.Context())
	if err != nil {
		log.WithFields(log.Fields{"error": err}).Error()
		http.Error(w, "Oops! Something went wrong in server.", http.StatusInternalServerError)
		return
	}

	a.sendJSON(w, http.StatusOK, customers)
}

//
// Product handlers
//

func (a *App) CreateProduct(w http.ResponseWriter, r *http.Request) {
	var product *evermos.Product
	err := json.NewDecoder(r.Body).Decode(&product)
	if err != nil {
		a.sendJSON(w, http.StatusBadRequest, map[string]interface{}{
			"error": map[string]interface{}{
				"message": err.Error(),
			},
		})
		return
	}

	err = a.ProductSrv.Create(r.Context(), product)
	if err != nil {
		log.WithFields(log.Fields{
			"product": product,
			"error":   err,
		}).Error()
		http.Error(w, "Oops! Something went wrong in server.", http.StatusInternalServerError)
		return
	}

	a.sendJSON(w, http.StatusCreated, product)
}

func (a *App) FindProductByID(w http.ResponseWriter, r *http.Request) {
	productID := chi.URLParam(r, "product_id")
	product, err := a.ProductSrv.FindByID(r.Context(), productID)
	switch {
	case errors.Is(err, evermos.ErrNotFound):
		a.sendJSON(w, http.StatusNotFound, map[string]interface{}{
			"error": map[string]interface{}{
				"message": err.Error(),
			},
		})
		return

	case err != nil:
		log.WithFields(log.Fields{
			"product_id": productID,
			"error":      err,
		}).Error()
		http.Error(w, "Oops! Something went wrong in server.", http.StatusInternalServerError)
		return
	}

	a.sendJSON(w, http.StatusOK, product)
}

func (a *App) FindProducts(w http.ResponseWriter, r *http.Request) {
	products, err := a.ProductSrv.FindMany(r.Context())
	if err != nil {
		http.Error(w, "Oops! Something went wrong in server.", http.StatusInternalServerError)
		return
	}

	a.sendJSON(w, http.StatusOK, products)
}

//
// CustomerOrder handlers
//

func (a *App) CreateCustomerOrder(w http.ResponseWriter, r *http.Request) {
	var order *evermos.CustOrder
	err := json.NewDecoder(r.Body).Decode(&order)
	if err != nil {
		a.sendJSON(w, http.StatusBadRequest, map[string]interface{}{
			"error": map[string]interface{}{
				"message": err.Error(),
			},
		})
		return
	}

	err = a.CustOrderSrv.Create(r.Context(), order)
	switch {
	case errors.Is(err, evermos.ErrNotFound):
		a.sendJSON(w, http.StatusNotFound, map[string]interface{}{
			"error": map[string]interface{}{
				"message": err.Error(),
			},
		})
		return

	case errors.Is(err, evermos.ErrInsufficientProductStock):
		a.sendJSON(w, http.StatusBadRequest, map[string]interface{}{
			"error": map[string]interface{}{
				"message": err.Error(),
			},
		})
		return

	case err != nil:
		log.WithFields(log.Fields{
			"order": order,
			"error": err,
		}).Error()
		http.Error(w, "Oops! Something went wrong in server.", http.StatusInternalServerError)
		return
	}

	a.sendJSON(w, http.StatusCreated, order)
}

func (a *App) FindCustOrderByID(w http.ResponseWriter, r *http.Request) {
	orderID := chi.URLParam(r, "order_id")
	order, err := a.CustOrderSrv.FindByID(r.Context(), orderID)
	switch {
	case errors.Is(err, evermos.ErrNotFound):
		a.sendJSON(w, http.StatusNotFound, map[string]interface{}{
			"error": map[string]interface{}{
				"message": err.Error(),
			},
		})
		return

	case err != nil:
		log.WithFields(log.Fields{
			"order_id": orderID,
			"error":    err,
		}).Error()
		http.Error(w, "Oops! Something went wrong in server.", http.StatusInternalServerError)
		return
	}

	a.sendJSON(w, http.StatusOK, order)
}

func (a *App) FindCustomerOrders(w http.ResponseWriter, r *http.Request) {
	orders, err := a.CustOrderSrv.FindMany(r.Context())
	if err != nil {
		log.WithFields(log.Fields{"error": err}).Error()
		http.Error(w, "Oops! Something went wrong in server.", http.StatusInternalServerError)
		return
	}

	a.sendJSON(w, http.StatusOK, orders)
}

func (a *App) sendJSON(w http.ResponseWriter, status int, data interface{}) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	_ = json.NewEncoder(w).Encode(data)
}
