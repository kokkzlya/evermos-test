package evermos

import (
	"context"
	"fmt"
	"time"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4"
)

type ProductService interface {
	Create(ctx context.Context, product *Product) error
	FindByID(ctx context.Context, id string) (*Product, error)
	FindMany(ctx context.Context) ([]*Product, error)
}

type DefaultProductService struct{}

func NewDefaultProductService() *DefaultProductService {
	return &DefaultProductService{}
}

func (s *DefaultProductService) Create(ctx context.Context, product *Product) (err error) {
	session := GetScopedSession(ctx)
	q := `
	INSERT INTO product (id, name, price, stock, created, updated)
	VALUES (
		$1, $2, $3,
		$4, $5, $6
	)`
	now := time.Now()
	product.ID = uuid.New().String()
	product.Created = now
	product.Updated = now
	_, err = session.Exec(
		ctx, q,
		product.ID, product.Name, product.Price,
		product.Stock, product.Created, product.Updated)

	return err
}

func (s *DefaultProductService) FindByID(ctx context.Context, id string) (*Product, error) {
	session := GetScopedSession(ctx)
	q := `
	SELECT id, name, price, stock, created, updated
	FROM product
	WHERE id = $1
	LIMIT 1`
	p := &Product{}
	row := session.QueryRow(ctx, q, id)
	err := row.Scan(&p.ID, &p.Name, &p.Price, &p.Stock, &p.Created, &p.Updated)
	switch {
	case err == pgx.ErrNoRows:
		return nil, fmt.Errorf("%w: Product with ID %s is not found", ErrNotFound, id)

	case err != nil:
		return nil, err
	}

	return p, nil
}

func (s *DefaultProductService) FindMany(ctx context.Context) ([]*Product, error) {
	session := GetScopedSession(ctx)
	q := `
	SELECT id, name, price, stock, created, updated
	FROM product`
	products := make([]*Product, 0)
	rows, err := session.Query(ctx, q)
	if err != nil {
		return nil, err
	}

	defer rows.Close()
	var p *Product
	for rows.Next() {
		p = &Product{}
		err = rows.Scan(&p.ID, &p.Name, &p.Price, &p.Stock, &p.Created, &p.Updated)
		if err != nil {
			return nil, err
		}

		products = append(products, p)
	}

	return products, nil
}
