package evermos

import (
	"time"
)

type ContextKey string

type Customer struct {
	ID       string    `json:"id"`
	Name     string    `json:"name"`
	Username string    `json:"username"`
	Email    string    `json:"email"`
	Created  time.Time `json:"created"`
	Updated  time.Time `json:"updated"`
}

type Product struct {
	ID      string    `json:"id"`
	Name    string    `json:"name"`
	Price   float64   `json:"price"`
	Stock   int       `json:"stock"`
	Created time.Time `json:"created"`
	Updated time.Time `json:"updated"`
}

type CustOrder struct {
	ID       string           `json:"id"`
	Customer *Customer        `json:"customer"`
	Items    []*CustOrderItem `json:"items"`
	Created  time.Time        `json:"created"`
	Updated  time.Time        `json:"updated"`
}

type CustOrderItem struct {
	ID      string     `json:"id"`
	Order   *CustOrder `json:"order"`
	Product *Product   `json:"product"`
	Qty     int        `json:"qty"`
	Price   float64    `json:"price"`
}
