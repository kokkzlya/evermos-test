package evermos

import (
	"context"
	"fmt"
	"strings"
	"time"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4"
)

type CustOrderService interface {
	Create(ctx context.Context, order *CustOrder) error
	FindByID(ctx context.Context, id string) (*CustOrder, error)
	FindMany(ctx context.Context) ([]*CustOrder, error)
}

type DefaultCustOrderService struct {
}

func NewDefaultCustOrderService() *DefaultCustOrderService {
	return &DefaultCustOrderService{}
}

func (s *DefaultCustOrderService) Create(ctx context.Context, order *CustOrder) (err error) {
	session := GetScopedSession(ctx)
	q := `SELECT id FROM customer WHERE id = $1 LIMIT 1`
	err = session.QueryRow(ctx, q, order.Customer.ID).Scan(&order.Customer.ID)
	switch {
	case err == pgx.ErrNoRows:
		return fmt.Errorf(
			"%w: Customer with ID: %s is not found",
			ErrNotFound, order.Customer.ID,
		)
	}

	now := time.Now()
	order.ID = uuid.New().String()
	order.Created = now
	order.Updated = now
	q = `
	INSERT INTO customer_order (id, customer_id, created, updated)
	VALUES ($1, $2, $3, $4)`
	_, err = session.Exec(ctx, q, order.ID, order.Customer.ID, order.Created, order.Updated)
	if err != nil {
		return err
	}

	if err = s.saveCustOrderItems(ctx, order); err != nil {
		return err
	}

	return err
}

func (s *DefaultCustOrderService) saveCustOrderItems(ctx context.Context, order *CustOrder) (err error) {
	session := GetScopedSession(ctx)

	// lock all related/ordered products
	ordersNum := len(order.Items)
	orderMap := make(map[string]*CustOrderItem)
	productIDs := make([]interface{}, ordersNum)
	pgParams := make([]string, ordersNum)
	for i, item := range order.Items {
		productID := item.Product.ID
		orderMap[productID] = item
		productIDs[i] = productID
		pgParams[i] = fmt.Sprintf("$%d", i+1)
	}

	q := `
	SELECT id, price, stock
	FROM product
	WHERE id IN (%s) FOR UPDATE`
	q = fmt.Sprintf(q, strings.Join(pgParams, ","))
	rows, err := session.Query(ctx, q, productIDs...)
	if err != nil {
		return err
	}

	defer rows.Close()
	xProducts := make(map[string]struct{})
	for rows.Next() {
		p := &Product{}
		err = rows.Scan(&p.ID, &p.Price, &p.Stock)
		if err != nil {
			return err
		}

		// check product's stock sufficiency
		if p.Stock < orderMap[p.ID].Qty {
			return fmt.Errorf(
				"%w: The stock of the product with ID: %s is insufficient",
				ErrInsufficientProductStock, p.ID,
			)
		}

		xProducts[p.ID] = struct{}{}
		orderMap[p.ID].Product = p
	}

	// check if all products exist
	for _, id := range productIDs {
		if _, ok := xProducts[id.(string)]; !ok {
			return fmt.Errorf(
				"%w: Product with ID: %s is not found",
				ErrNotFound, id,
			)
		}
	}

	batch := &pgx.Batch{}
	for _, item := range order.Items {
		q := `
		UPDATE product
		SET stock = stock - $1
		WHERE id = $2`
		batch.Queue(q, item.Qty, item.Product.ID)
	}

	// save order items
	for i, item := range order.Items {
		item.ID = fmt.Sprintf("%d", i)
		q := `
		INSERT INTO customer_order_item (id, order_id, product_id, quantity, price)
		VALUES ($1, $2, $3, $4, $5)`
		batch.Queue(q, item.ID, order.ID, item.Product.ID, item.Qty, item.Product.Price)
	}

	br := session.(*PgScopedSession).Tx.SendBatch(ctx, batch)
	defer br.Close()
	_, err = br.Exec()
	return err
}

func (s *DefaultCustOrderService) FindByID(ctx context.Context, id string) (*CustOrder, error) {
	session := GetScopedSession(ctx)
	q := `
	SELECT id, customer_id, created, updated
	FROM customer_order
	WHERE id = $1
	LIMIT 1`
	order := &CustOrder{
		Customer: &Customer{},
	}
	err := session.QueryRow(ctx, q, id).
		Scan(&order.ID, &order.Customer.ID, &order.Created, &order.Updated)
	switch {
	case err == pgx.ErrNoRows:
		return nil, fmt.Errorf("%w: Customer order with ID: %s is not found", ErrNotFound, id)

	case err != nil:
		return nil, err
	}

	q = `
	SELECT oi.id, oi.quantity, oi.price, p.id, p.name
	FROM customer_order_item oi
	JOIN product AS p ON oi.product_id = p.id
	WHERE oi.order_id = $1`
	rows, err := session.Query(ctx, q, id)
	if err != nil {
		return nil, err
	}

	defer rows.Close()
	items := make([]*CustOrderItem, 0)
	for rows.Next() {
		item := &CustOrderItem{Product: &Product{}}
		var itemID int
		err = rows.Scan(&itemID, &item.Qty, &item.Price, &item.Product.ID, &item.Product.Name)
		if err != nil {
			return nil, err
		}

		item.ID = fmt.Sprintf("%d", itemID)
		items = append(items, item)
	}

	order.Items = items
	return order, nil
}

func (s *DefaultCustOrderService) FindMany(ctx context.Context) ([]*CustOrder, error) {
	session := GetScopedSession(ctx)
	q := `
	SELECT id, customer_id, created, updated
	FROM customer_order`
	rows, err := session.Query(ctx, q)
	if err != nil {
		return nil, err
	}

	defer rows.Close()
	custOrders := make([]*CustOrder, 0)
	var custOrder *CustOrder
	for rows.Next() {
		custOrder = &CustOrder{
			Customer: &Customer{},
		}
		err = rows.Scan(
			&custOrder.ID, &custOrder.Customer.ID, &custOrder.Created, &custOrder.Updated,
		)
		if err != nil {
			return nil, err
		}

		custOrders = append(custOrders, custOrder)
	}

	return custOrders, nil
}
