package evermos

import (
	"errors"
)

var (
	ErrNotFound                 = errors.New("Not found error")
	ErrInsufficientProductStock = errors.New("Insufficient product stock error")
)
