package evermos

import (
	"context"
	"fmt"
	"time"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4"
)

type CustomerService interface {
	Create(ctx context.Context, cust *Customer) error
	FindByUsername(ctx context.Context, username string) (*Customer, error)
	FindMany(ctx context.Context) ([]*Customer, error)
}

type DefaultCustomerService struct {
}

func NewDefaultCustomerService() *DefaultCustomerService {
	return &DefaultCustomerService{}
}

func (s *DefaultCustomerService) Create(ctx context.Context, c *Customer) (err error) {
	session := GetScopedSession(ctx)
	now := time.Now()
	c.ID = uuid.New().String()
	c.Created = now
	c.Updated = now
	q := `
	INSERT INTO customer (id, name, username, email, created, updated)
	VALUES ($1, $2, $3, $4, $5, $6)`
	_, err = session.Exec(
		ctx, q,
		c.ID, c.Name, c.Username, c.Email, c.Created, c.Updated,
	)

	return err
}

func (s *DefaultCustomerService) FindByUsername(ctx context.Context, username string) (*Customer, error) {
	session := GetScopedSession(ctx)
	q := `
	SELECT id, name, username, email, created, updated
	FROM customer
	WHERE username = $1
	LIMIT 1`
	c := &Customer{}
	err := session.QueryRow(ctx, q, username).
		Scan(&c.ID, &c.Name, &c.Username, &c.Email, &c.Created, &c.Updated)
	switch {
	case err == pgx.ErrNoRows:
		return nil, fmt.Errorf("%w: Customer with username: %s is not found", ErrNotFound, username)
	}

	return c, err
}

func (s *DefaultCustomerService) FindMany(ctx context.Context) ([]*Customer, error) {
	session := GetScopedSession(ctx)
	q := `
	SELECT id, name, username, email, created, updated
	FROM customer`
	customers := make([]*Customer, 0)
	var c *Customer
	rows, err := session.Query(ctx, q)
	if err != nil {
		return nil, err
	}

	defer rows.Close()
	for rows.Next() {
		c = &Customer{}
		err = rows.Scan(
			&c.ID, &c.Name, &c.Username, &c.Email, &c.Created, &c.Updated,
		)
		if err != nil {
			return nil, err
		}

		customers = append(customers, c)
	}

	return customers, nil
}
