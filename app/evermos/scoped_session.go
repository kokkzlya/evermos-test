package evermos

import (
	"context"
	"net/http"
	"reflect"

	"github.com/jackc/pgconn"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	log "github.com/sirupsen/logrus"
)

var (
	CKScopedSession ContextKey = "CKScopedSession"
)

//
// ScopedSession
//

type ScopedSession interface {
	Begin(ctx context.Context) (pgx.Tx, error)
	Exec(ctx context.Context, query string, args ...interface{}) (pgconn.CommandTag, error)
	Query(ctx context.Context, query string, args ...interface{}) (pgx.Rows, error)
	QueryRow(ctx context.Context, query string, args ...interface{}) pgx.Row
}

//
// PgScopedSession
//

type PgScopedSession struct {
	Conn *pgxpool.Pool
	Tx   pgx.Tx
}

func (s *PgScopedSession) Begin(ctx context.Context) (pgx.Tx, error) {
	if s.Tx != nil {
		_ = s.Tx.Rollback(ctx)
	}

	var err error
	if s.Tx, err = s.Conn.Begin(ctx); err != nil {
		return nil, err
	}

	return s.Tx, nil
}

func (s *PgScopedSession) Exec(ctx context.Context, query string, args ...interface{}) (pgconn.CommandTag, error) {
	return s.Tx.Exec(ctx, query, args...)
}

func (s *PgScopedSession) Query(ctx context.Context, query string, args ...interface{}) (pgx.Rows, error) {
	return s.Tx.Query(ctx, query, args...)
}

func (s *PgScopedSession) QueryRow(ctx context.Context, query string, args ...interface{}) pgx.Row {
	return s.Tx.QueryRow(ctx, query, args...)
}

//
// ScopedSession's factory
//

type ScopedSessionFactory interface {
	Create(conn *pgxpool.Pool) ScopedSession
}

type PgScopedSessionFactory struct {
}

func (f *PgScopedSessionFactory) Create(conn *pgxpool.Pool) ScopedSession {
	return &PgScopedSession{Conn: conn}
}

//
// gochi's middleware
//

func CreateScopedSessionMiddleware(f ScopedSessionFactory, conn *pgxpool.Pool) func(http.Handler) http.Handler {
	fn := func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			s := f.Create(conn)
			tx, err := s.Begin(r.Context())
			if err != nil {
				log.WithFields(log.Fields{"error": err}).Error("Failed to begin transaction")
				http.Error(w, "500 Internal Server Error", http.StatusInternalServerError)
				return
			}

			defer func() {
				if err := recover(); err != nil {
					log.WithFields(log.Fields{"error": err}).Error("Failed to handle request")
					_ = tx.Rollback(r.Context())
					http.Error(w, "500 Internal Server Error", http.StatusInternalServerError)
					return
				}

				statusCode := getStatusCode(w)
				if statusCode < 400 {
					if err := tx.Commit(r.Context()); err == nil {
						return
					}

					log.WithFields(log.Fields{"error": err}).Error("Failed to commit the transaction")
				}

				_ = tx.Rollback(r.Context())
			}()

			ctx := context.WithValue(r.Context(), CKScopedSession, s)
			next.ServeHTTP(w, r.WithContext(ctx))
		})
	}

	return fn
}

func GetScopedSession(ctx context.Context) ScopedSession {
	value := ctx.Value(CKScopedSession)
	if session, ok := value.(ScopedSession); ok {
		return session
	}

	return nil
}

func getStatusCode(w http.ResponseWriter) int {
	value := reflect.ValueOf(w).Elem()
	field := value.FieldByName("status") // from *http.response
	if field.IsValid() {
		return int(field.Int())
	}

	field = value.FieldByName("Code") // maybe from *httptest.ResponseRecorder
	if field.IsValid() {
		return int(field.Int())
	}

	return 0
}
