# Evermos
## Background
We are members of the engineering team of an online store. When we look at ratings for our online store application, we received the following facts:
1. Customers were able to put items in their cart, check out, and then pay. After several days, many of our customers received calls from our Customer Service department stating that their orders have been canceled due to stock unavailability.
1. These bad reviews generally come within a week after our 12.12 event, in which we held a large flash sale and set up other major
discounts to promote our store.

After checking in with our Customer Service and Order Processing departments, we received the following additional facts:
1. Our inventory quantities are often misreported, and some items even go as far as having a negative inventory quantity.
1. The misreported items are those that performed very well on our 12.12 event.
1. Because of these misreported inventory quantities, the Order Processing department was unable to fulfill a lot of orders, and thus
requested help from our Customer Service department to call our customers and notify them that we have had to cancel their orders.

## Assumption
There are some assumptions in my mind about the issue. First, the issue is about database replication. But, I think the DBMS should be smart enough to handle the data synchronization between replicas. If the problem is really comes from the database replication things, this is not my scope. This must be the Database Administation (DBA) business.

My other suspicion is about database transaction and affected row(s) locking issue. But this suspicion weakened because I believe that developers at Evermos are good and they should have known about the use of locking the row(s) within the transaction section. This is enough to prevent race condition to decreasing a product's stock value. Basically, start the transaction, select all ordered product and lock it using `FOR UPDATE` clauses (if you use SQL DBMS), check for the products' stock. If all stock is sufficient, then save the order, bulk update the products' stock, and bulk save the order items, and then commit the transaction if everything there is no error. This way should prevent the race condition and negative stock issue. I'll demonstrate in this project that the row locking can prevent race condition to the decreasing of the product's stock value during the order process.

I can't tell much about this issue. The only descriptive text cannot tell much about what really happened inside the application. I need to see the code to get more information about it.

## Proof of concept
I have no proof of concept due to my assumptions written above was already weakened. Even if we want to lively show the stock information for every products to our clients in their pages, it will be very ineffective. We need more resource to serve every product's stock information lively to every online clients, and also it will still cause the race condition because there will be a delay when click the order request, sending it, and process the order request in backend. Consider about the use of bots too. The only solution is the use of transaction and row(s) locking.

This project contains the code that only demonstrate the use of transaction and row(s) locking can effectively enough prevent race condition as describe above. I also created unittests to simulate the case to make sure that many requests at the same time can be queued using transaction and row(s) locking, so the products' stock negative value issue won't be occured. The tests also only focused for product orderings race condition issue.

## How to run

1. Change directory to `docker` directory:
    ```
    cd docker
    ```

1. Run postgresql service:
    ```
    docker-compose up -d postgresql
    ```

1. Run migration
    ```
    docker-compose exec -T postgresql psql -U evermos -d evermos < initdb.sql
    ```

1. Build the web API container
    ```
    docker-compose build evermos
    ```

1. Run web API
    ```
    docker-compose up -d evermos
    ```

1. Try to request to the server
    ```
    $ curl -i -X GET http://localhost:3000/api

    HTTP/1.1 200 OK
    Content-Type: application/json
    Date: Thu, 13 May 2021 05:07:31 GMT
    Content-Length: 34

    {"message":"Konnichi-wa, Sekai!"}
    ```
    To try ordering item
    ```
    $ curl -i -X POST http://localhost:3000/api/customers -d '{
        "customer": {
            "id": "3ab744cf-0a00-4b5c-840a-25ed92ddbeec"
        },
        "items": [
            {
                "product": {
                    "id": "f572a296-6be2-47ab-b24c-4538b3b014bb"
                },
                "qty": 100
            }
        ]
    }'
    ```


These are the available endpoints:

| Method | Endpoint                     | Description              |
| ------ | ---------------------------- | ------------------------ |
| `POST` | `/api/customers`             | Create new customer      |
| `GET`  | `/api/customers`             | Get all customers        |
| `GET`  | `/api/customers/{username}`  | Get customer by username |
| `POST` | `/api/products`              | Create new product       |
| `GET`  | `/api/products`              | Get all products         |
| `GET`  | `/api/products/{product_id}` | Get product by ID        |
| `POST` | `/api/orders`                | Create order             |
| `GET`  | `/api/orders`                | Get all orders           |
| `GET`  | `/api/orders/{order_id}`     | Get order by ID          |


## How to run test
Caution: Running the test may remove all records in the database.

1. Change directory to `docker` directory:
    ```
    cd docker
    ```

1. Run postgresql service:
    ```
    docker-compose up -d postgresql
    ```

1. Run migration
    ```
    docker-compose exec -T postgresql psql -U evermos -d evermos < initdb.sql
    ```

1. Run test
    ```
    docker-compose up test
    ```
